$( document ).ready(function() {
	var baselink = $('#baselink').val();
	countInvoice();
	setInterval(function(){ 
		countInvoice();
	
	}, 5000);
	var idPekerjaanKlien = $("#idPekerjaan").val();
	function countInvoice(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/new',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){				
				var data = jQuery.parseJSON(response);
				if (data.length > 0){
					$("#invoiceCount").html(data.length);
				} else {
					$("#invoiceCount").html('');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		       	console.log(xhr.status);
		       	console.log(xhr.responseText);
		       	console.log(thrownError);
			}
		});			
	}
	$("#invoicePanel").on('click',function(){
		window.location.href = baselink+'invoice';
	});
	$("#confirmPanel").on('click',function(){
		window.location.href = baselink+'confirm';
	});	
	$("#saranPanel").on('click',function(){
		window.location.href = baselink+'saran';
	});	
	$('#viewList').on('click','#itemRecruit',function(e) { 
			var url = baselink+'recruit';
			window.location.href = url;
	});	
	$('#viewList').on('click','#itemPenggajian',function(e) { 
			var url = baselink+'penggajian';
			window.location.href = url;
	});	
	$('#viewList').on('click','#itemHrd',function(e) { 
			var url = baselink+'hrd';
			window.location.href = url;
	});	
	$('#viewList').on('click','#itemSOP',function(e) { 
			var url = baselink+'sop';
			window.location.href = url;
	});			
});