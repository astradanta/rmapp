<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Recruitment <?php echo($namaKlien); ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo(base_url()."service/".$this->uri->segment(2)); ?>"><?php echo $namaKlien; ?></a></li>
        <li class="active">Recruitment</li>
        <input type="hidden" name="" value="<?php echo($id_client); ?>" id="idClient">
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding-top: 20px">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Daftar Request</a></li>
              <li><a href="#tab_2" data-toggle="tab">Working Board</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1" >
                <div class="row">
                  <div class="col-md-12">
                  <table id="tableRequest" class="table table-bordered table-striped">
                    <thead>
                          <tr>
                            <th width="15%">Posisi</th>
                            <th width="10%">Gaji</th>
                            <th width="12%">Jumlah</th>
                            <th width="20%">Syarat/Ketentuan</th>
                            <th width="12%">Deadline</th>
                            <th width="10%">Status</th>                            
                            <th width="70">Aksi</th>
                          </tr>
                    </thead>
                    <tbody id="listView">
                    </tbody>
                  </table>                  
                  </div>
                </div>  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="row">                    
                    <div class="col-xs-12" id="workingView">
                    
                    </div>
                  </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>              
        </div>
      </div>
    </section>	
</div>
<div class="modal fade" id="manipulateModal">
  <form action="<?php echo base_url(); ?>recruit/change" method="post" id="manipulateForm">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Request</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="idDetail" value="">
                  <input type="hidden" name="deadline" id="deadline">
                  <input type="hidden" name="id_client" value="<?php echo($id_client); ?>">
                  <div class="form-group">
                    <label>Status Request</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="firstRadio" name="status" value="1">Disetujui</label>
                          <label class="radio-inline"><input type="radio" id="secondRadio" name="status" value="2">Tidak Disetujui</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="changeButton" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>
<div style="display: none;" id="listItemTemplate">
            <div class="col-md-6 col-xs-12">
              <div class="info-box bg-aqua infoBox" style="cursor: pointer;" >
                <span class="info-box-icon infoBoxIcon" style="" id="itemLeftIcon" data-id=""><i style="font-size: 40px;">100<sup>%</sup></i></span>

                <div class="info-box-content ">
                  <div style="display: flex; height: 44px;" id="itemContainer" data-id="">
                    <span style="white-space: normal;" class="info-box-text col-xs-11" id="namaPekerjaan">SPT Masa (bulanan)</span>
                    <span class="info-box-number col-xs-1"><i style="line-height: 44px" class="pull-right fa fa-fw fa-arrow-right"></i></span>
                  </div>
                  <div class="progress col-xs-12">
                    <div class="progress-bar" style="width: 70%"></div>
                  </div>
                  <div class="col-xs-11" style="display: flex">
                    <span class="info-box-text col-xs-11" id="itemlistDeadline">2018-06-17</span>
                  </div>
                  
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>  
</div> 
<div class="modal fade" id="detailModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
              </div>
              <form action="" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Posisi</label>
                    <input type="text" class="form-control" name="nama" id="detailPosisi" placeholder="Ketikan Nama" required="" disabled="">
                  </div>
                  <div class="form-group">
                    <label>Gaji </label>
                     <input type="text" class="form-control" name="nama" id="detailGaji" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div> 
                  <div class="form-group">
                    <label>Jumlah </label>
            <input type="text" class="form-control" name="nama" id="detailJumlah" placeholder="Ketikan Nama" required="" disabled="">  
                  </div>                                                                                          
                  <div class="form-group">
                    <label for="exampleInputPassword1">Syarat dan Ketentuan</label>
                    <div class="custom-form-control" id="detailSyarat"></div>
                  </div>
                  <div class="form-group">
                    <label>Status </label>
                    <input type="text" class="form-control" name="nama" id="detailStatus" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Deadline</label>
            <input type="text" class="form-control" name="nama" id="detailDeadline" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Dibuat Pada </label>
            <input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
            <input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div>                  
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                     <label id="editGenerate"></label>                    
                  </div>                                    
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>  