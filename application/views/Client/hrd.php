<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan HRD
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Laporan HRD</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row" style="padding-top: 20px;">
        <div class="col-xs-12">
          
          <!-- /.box -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Laporan HRD</h3>
            </div>
            <div class="box-body">            
              <table id="tableHrd" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20%">Tanggal</th>
                  <th width="20%">Periode</th>
                  <th width="20%">Catatan</th>
                  <th width="20%">Status</th>
                  <th width="20%">Aksi</th>
                </tr>
                </thead>
                <tbody id="listView">
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Form Tambah Data Laporan HRD</h3>
              </div>
              <form action="<?php echo(base_url()) ?>hrd/add" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <input type="hidden" name="id_kerja" id="idDetail">
                  
                    <label for="exampleInputEmail1">Periode</label>
                    <input type="text" class="form-control form-control-1 input-sm" name="periode" id="periode" autocomplete="off" placeholder="" required="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Catatan</label>
                      <textarea id="catatan" name="catatan" rows="10" cols="80">
                      </textarea>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">URL</label>
                    <input type="text" class="form-control" name="url" id="url" placeholder="" required="">
                  </div>                                   
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div id="itemAction" style="display: none;">
  <button class="btn btn-info" style="margin: 2px" id="itemDownload"><i class="fa fa-download"></i> </button>
</div>  
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>