<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_penggajian extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function userDataAbsensi($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 2 AND tb_detail_kerja.id_detail_pekerjaan = 5';
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function adminDataAbsensi($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 2 AND tb_detail_kerja.id_detail_pekerjaan = 5 AND tb_kerja.status = "on progress"';
			$query = $this->db->query($sql);
			return $query->result();			
		}		
		function userAbsensiDetail($id_client,$id_kerja){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 2 AND tb_detail_kerja.id_detail_pekerjaan = 5 AND tb_kerja.id_kerja = '.$id_kerja;
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function userAbsensiEdit($id_kerja,$startDate,$deadLine,$updated_at,$periode,$note,$url){
			$data = array("start_date"=>$startDate,"deadline"=>$deadLine,"updated_at"=>$updated_at,"periode"=>$periode);
			$this->db->where("id_kerja",$id_kerja);
			$edit = $this->db->update("tb_kerja",$data);
			if($edit){

				$this->editDetailKerja($id_kerja,$note,$url);
			}
			return $edit;
		}
		function editDetailKerja($id_kerja,$note,$url){
			$data = array("note"=>$note,"upload_file"=>$url);
			$this->db->where("id_kerja",$id_kerja);
			$this->db->where("id_detail_pekerjaan",5);
			$this->db->update("tb_detail_kerja",$data);
		}
		function addDetailKerja($idPekerjaan,$startDate,$deadLine,$created_at,$periode){
			$data = array("id_daftar_pekerjaan_client"=>$idPekerjaan,"start_date"=>$startDate,"deadline"=>$deadLine,"created_at"=>$created_at,"status"=>"on progress","periode"=>$periode);
			$insert =  $this->db->insert('tb_kerja',$data);
			return $this->db->insert_id();
		}
		function deleteKerja($id_kerja){
			$detail = $this->deleteDetail($id_kerja);
			$delete = 0;
			if($detail){
				$this->db->where("id_kerja",$id_kerja);
				$delete = $this->db->delete("tb_kerja");
			}
			return $delete;
		}
		function deleteDetail($id_kerja){
			$this->db->where("id_kerja",$id_kerja);
			return $this->db->delete("tb_detail_kerja");
		}
		function deleteDetailSlip($id_kerja){
			$this->db->where("id_kerja",$id_kerja);
			$this->db->where("id_detail_pekerjaan",6);
			return $this->db->delete("tb_detail_kerja");
		}		
		function changeAbsensiStatus($id_kerja,$status){
			$data = array("status"=>$status);
			$this->db->where('id_kerja',$id_kerja);
			$this->db->where('id_detail_pekerjaan',5);
			return	$this->db->update("tb_detail_kerja",$data);
		}
		function pickerSlipGaji($id_client){
			$this->db->where('id_client',$id_client);
			$this->db->where('id_pekerjaan',2);
			$this->db->where('total_dikerjakan',1);
			return $this->db->get("r_kerja_detail_kerja")->result();
		}
		function insertSlip($id_kerja,$catatan,$upload_file){
			$created_at = date('Y-m-d');
			$data = array("id_detail_pekerjaan"=>6,"note"=>$catatan,"upload_file"=>$upload_file,"id_kerja"=>$id_kerja,"created_at"=>$created_at,"status"=>1);
			return $this->db->insert("tb_detail_kerja",$data);

		}
		function userDataSlip($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 2 AND tb_detail_kerja.id_detail_pekerjaan = 6';
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function adminDataSlip($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 2 AND tb_detail_kerja.id_detail_pekerjaan = 6 AND tb_kerja.status = "on progress"';
			$query = $this->db->query($sql);
			return $query->result();			
		}		
		function userSlipDetail($id_kerja){
			$this->db->select('tb_detail_kerja.*,tb_kerja.periode');
			$this->db->from('tb_detail_kerja');
			$this->db->join('tb_kerja','tb_detail_kerja.id_kerja = tb_kerja.id_kerja');
			$this->db->where('tb_detail_kerja.id_kerja',$id_kerja);
			$this->db->where('tb_detail_kerja.id_detail_pekerjaan',6);
			return $this->db->get()->result();
		}
		function userSlipEdit($id_detail_kerja,$id_kerja,$catatan,$url){
			$updated_at = date("Y-m-d");
			$data = array("id_kerja"=>$id_kerja,"note"=>$catatan,"upload_file"=>$url,"updated_at"=>$updated_at);
			$this->db->where("id_detail_kerja",$id_detail_kerja);
			return $this->db->update("tb_detail_kerja",$data);
		}
	}
?>