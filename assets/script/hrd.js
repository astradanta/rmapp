$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_client = $("#idClient").val();
  $("#periode").datepicker({
    autoclose: true,
    minViewMode: 1,
    language: "id",
    locale: "id",
    format: 'MM yyyy'
	}).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('.to').datepicker('setStartDate', startDate);
    });
    CKEDITOR.replace('catatan');
    CKEDITOR.instances.catatan.updateElement();
    list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'hrd/list',
			data:{'id_client':id_client},
			cache: false,
			success: function(response){
				console.log(response);
                var data = jQuery.parseJSON(response);
                $("#listView").html("");
                $.each(data,function(i,item){
                    parseList(item);
                });
                $("#tableHrd").DataTable({autoWidth:false,scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){

        var text =  '<tr>'+
                        '<td>'+data.tanggal+'</td>'+
                        '<td>'+data.periode+'</td>'+
                        '<td>'+data.note+'</td>'+
                         '<td>'+data.status+'</td>';
        $("#itemAction").find("#itemEdit").attr("data-id",data.id_kerja);
        $("#itemAction").find("#itemDelete").attr("data-id",data.id_kerja);
        $("#itemAction").find("#itemDownload").attr("data-url",data.upload_file);
        $("#itemAction").find("#itemFinish").attr("data-id",data.id_kerja);
        text +="<td style='text-align:center'>"+$("#itemAction").html()+"</td></tr>";
        $("#listView").append(text);
    }  
    $("#listView").on('click','#itemEdit',function(){
        var id = $(this).attr('data-id');
        detail(id);
    });
 	function detail(id){
        $.ajax({
            type: "POST",
            url: baselink+'hrd/detail',
            data:{"id_kerja":id,"id_client":id_client},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                parseDetail(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    function parseDetail(data){
        $("#periode").val(data[0].periode);
        $("#url").val(data[0].upload_file);
        $("#idDetail").val(data[0].id_kerja);
        CKEDITOR.instances.catatan.setData(data[0].note);
        $("#modalTitle").html("Form Ubah Data Laporan HRD");
        $("#manipulateForm").prop("action",baselink+"hrd/edit");
        $("#manipulateModal").modal("show");
    }
    $("#manipulateModal").on("hidden.bs.modal", function () {
        resetModal();
    });
    function resetModal(){
        $("input[type=text]").val("");
        CKEDITOR.instances.catatan.setData("");
        $("#modalTitle").html("Form Tambah Data Absensi");
        $("#manipulateForm").prop("action",baselink+"penggajian/add");
    } 
    $("#listView").on('click','#itemDelete',function(e){
        e.preventDefault()
        var id = $(this).attr('data-id');
        $("#modal_delete").modal("show");
        $("#btn_modal").attr('data-id',id);
    }); 
    $("#btn_modal").click(function(){
        var id = $(this).attr("data-id")
        $.ajax({
            type: "POST",
            url: baselink+'hrd/delete',
            data:{"baselink":baselink,"id":id},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                if (data.status == "success"){
                    alert("Data Berhasil dihapus");
                    $("#modal_delete").modal("hide");
                    $("#tableHrd").DataTable().destroy();
                    list()
                } else {
                    alert("Gagal menghapus data")
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#manipulateForm").submit(function(e){
        e.preventDefault();
        CKEDITOR.instances.catatan.updateElement();
        console.log(CKEDITOR.instances.catatan.getData())
            $.ajax({
                type: "POST",
                url: $(this).prop("action"),
                data: new FormData( this ),
                processData: false,
                contentType: false,
                success: function(response){                    
                    var data = jQuery.parseJSON(response);
                    if (data.status == "success"){
                        alert("Data absensi berhasil disimpan");
                        $("#manipulateModal").modal("hide");
                        $("#tableHrd").DataTable().destroy();
                        list();
                    }else{
                        alert("Gagal menyimpan data absensi");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });         
        
    }); 
    $("#listView").on('click','#itemDownload',function(){
        var url = $(this).attr('data-url');
        window.open(url, 'name');       
    });         		
    $("#listView").on('click','#itemFinish',function(){
        var id = $(this).attr('data-id');
        finishIt(id)      ;
    });
    function finishIt(id)   {
         $.ajax({
            type: "POST",
            url: baselink+'hrd/change',
            data:{"id_kerja":id},
            cache: false,
            success: function(response){
                var data = jQuery.parseJSON(response);
                if(data.status == "success"){
                    alert("Berhasil mengubah status menjadi selesai");
                    $("#tableHrd").DataTable().destroy();
                    list();
                } else {
                    alert("Gagal mengubah status");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });     
    }	
});