<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Client Service Board
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Client Service Board</li>
        <li class="active"><?php echo $namaKlien; ?></li>
        <input type="hidden" name="" id="idPekerjaanKlien" value="<?php echo($idPekerjaanKlien) ?>">
      </ol>
      <div class="col-xs-12 contentHeader"></div>
    </section>
    <section class="content">
    	<div class="row" id="menuContainer">
	        <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box2" id="invoicePanel" style="cursor: pointer;">
	            <span class="info-box-icon2 bg-yellow"><i class="fa fa-group"></i></span>

	            <div class="info-box-content2">
	              <span class="info-box-text"><b>Recruitment</b></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	        </div>
	        <!-- /.col -->
	        <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box2" id="confirmPanel" style="cursor: pointer;">
	            <span class="info-box-icon2 bg-green"><i class="fa fa-dollar"></i></span>

	            <div class="info-box-content2">
	              <span class="info-box-text" style="margin-top: 43px;height: 50px;line-height: 23px;white-space:  normal;"><b>Penggajian</b></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	        </div>
	        <!-- /.col -->

	        <!-- fix for small devices only -->
	        <div class="clearfix visible-sm-block"></div>

	        <div class="col-md-3 col-sm-6 col-xs-12" >
	          <div class="info-box2" id="saranPanel" style="cursor: pointer;">
	            <span class="info-box-icon2 bg-blue"><i class="fa fa-file-text"></i></span>

	            <div class="info-box-content2">
	              <span class="info-box-text"><b>Laporan HRD</b></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	        </div>
	        <!-- /.col -->
	        <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box2" id="sopPanel" style="cursor: pointer;">
	            <span class="info-box-icon2 bg-red"><i class="fa fa-list"></i></span>

	            <div class="info-box-content2">
	              <span class="info-box-text"><b>Penyusunan SOP</b></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	        </div>    		
    	</div>
    	<div class="row" style="padding-top: 20px" id="clientInfoContainer">
				 <div class="col-md-4">
					          <!-- Widget: user widget style 1 -->
					          <div class="box box-widget widget-user timbul">
					            <!-- Add the bg color to the header using any of the bg-* classes -->
					            <div class="widget-user-header bg-aqua-active">
					              <h3 class="widget-user-username"><?php echo($namaKlien) ?></h3>
					            </div>
					            <div class="widget-user-image">
					              <img class="img-circle" src="<?php echo(base_url().$client->logo_client) ?>" alt="User Avatar" style="height: 80px;width: 80px;">
					            </div>
					            <div class="box-footer" style="background-color: #f8f8f8">
					              <div class="row">
					                <!-- /.col -->
					              </div>
					              <!-- /.row -->
					            </div>
					          </div>

					 </div> 
				        <div class="col-md-8">
				          
				          <!-- /.box -->

				          <div class="box box-primary timbul">
				            <div class="box-header">
				              <h3 class="box-title">Client Detail Info</h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Jenis Perusahaan</label>
				                  <input type="text" class="form-control" name="jenis" id="inputJenis" disabled="" value="<?php echo($client->jenis_perusahaan)?>" >
				                </div>
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Alamat</label>
				                  <textarea id="inputAlamat" name="alamat" class="form-control" disabled=""><?php echo($client->alamat)?></textarea>
				                </div>
				               	<div class="form-group">
				                  <label for="exampleInputEmail1">No Telepon</label>
				                  <input type="text" class="form-control" name="telepon" id="inputPhone" disabled="" value="<?php echo($client->no_telepon)?>">
				                </div>
				                <div class="form-group" >
				                  <label for="exampleInputEmail1">Email</label>
				                  <input type="email" class="form-control" name="email" id="inputEmail" disabled="" value="<?php echo($client->email)?>">
				                </div>
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Deskripsi</label>
				                  <textarea id="inputDeskripsi" name="deskripsi" class="form-control" disabled=""><?php echo($client->deskripsi)?></textarea>
				                </div>
				                <div class="form-group" >
				                  <label for="exampleInputEmail1">Mulai Kontrak</label>
				                  <input type="text" name="startContract" class="form-control" id="startContract" value="<?php echo($client->start_contract)?>" disabled="">
				                </div>
				               	<div class="form-group" >
				                  <label for="exampleInputEmail1">Selesai Kontrak</label>
				                  <input type="text" name="endContract" class="form-control" id="endContract" value="<?php echo($client->end_contract)?>" disabled="">
				                </div>   			                				            	
				            </div>
				            <!-- /.box-body -->
				          </div>
				          <!-- /.box -->
				        </div>	    		
    	</div>
    </section>	
</div>