<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Sop extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_recruit','',TRUE);
		$this->load->model('m_sop','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{						
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$id_pekerjaan = 4;
			$data["id_client"] = $id_client;
			$pekerjaanKlien = $this->m_pekerjaan_client->getPekerjaanKlienByuser($id_client,4);			
			$id_daftar_pekerjaan_client = $pekerjaanKlien[0]->id_daftar_pekerjaan_client;
			$cek = $this->m_sop->cekSop($id_daftar_pekerjaan_client);
			if($cek == 0){
				$this->load->view('Client/static/header');
				$this->load->view('Client/static/navbar');
				$this->load->view('Client/nodata');
				$this->load->view('Client/static/footer');					
			} else{
				$data["id_pekerjaan"] = $id_pekerjaan;
				$tempClient = $this->m_client->getDetailClient($id_client);
				$temp = $this->m_master->getDetailDataPekerjaan($data["id_pekerjaan"]);
				$data["namaPekerjaan"] = $temp[0]->nama_pekerjaan;	
				$data['namaClient']		= $tempClient[0]->nama_client;
				$this->load->view('Client/static/header',$data);
				$this->load->view('Client/static/navbar');
				$this->load->view('Client/sop');
				$this->load->view('Client/static/footer');	
			}					
	}
	function change(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])){
			$id = $_POST['id'];
			$status = $_POST['status'];
			$change = $this->m_sop->changeStatus($id,$status);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Mengganti status pekerjaan menjadi '.$status,$date);
			}
		}
		echo json_encode($result);
	}
	function display(){
		if(isset($_POST['id_client'])&&isset($_POST['id_pekerjaan'])){
			$id_client = $_POST['id_client'];
			$id_pekerjaan = $_POST['id_pekerjaan'];
			$data = $this->m_sop->display($id_client,$id_pekerjaan);
			$generalData['count'] = sizeof($data);
			if ($generalData['count']> 0){
				$generalData['start_date'] = $data[0]->start_date;
				$generalData['deadline'] = $data[0]->deadline;
				$generalData['status'] = $data[0]->status;
				$generalData['id_kerja'] = $data[0]->id_kerja;
				$itemData = $this->m_pekerjaan_client->getItemOnProgress($data[0]->id_kerja,$id_pekerjaan);
				$generalData['itemData'] = $itemData;
			}
			echo json_encode($generalData);
		} else {
			var_dump($_POST);
		}	
	}	
}	
