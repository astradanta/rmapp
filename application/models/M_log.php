<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_log extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function insertLog($id,$activity,$date){
			$data = array('id_user'=>$id,'activity'=>$activity,'date'=>$date);
			return $this->db->insert('tb_log_activity',$data);
		}
		function loadLogUser($id){
			$this->db->select('tb_log_activity.*,tb_user.nama');
			$this->db->from('tb_log_activity');
			$this->db->join('tb_user','tb_log_activity.id_user = tb_user.id_user');
			$this->db->where('tb_log_activity.id_user',$id);
			$this->db->order_by('tb_log_activity.date','desc');
			return $this->db->get()->result();
		}
		function currentLog($id){
			$this->db->select('tb_log_activity.*,tb_user.nama');
			$this->db->from('tb_log_activity');
			$this->db->join('tb_user','tb_log_activity.id_user = tb_user.id_user');
			$this->db->where('tb_log_activity.id_user',$id);
			$date = date('Y-m-d');
			$this->db->where('date',$date);
			$this->db->order_by('tb_log_activity.date','desc');
			return $this->db->get()->result();			
		}
	}
?>