<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_receipt extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function list(){
			$this->db->select('tb_receipt.*,tb_user.nama,tb_invoice.no_invoice');
			$this->db->from('tb_receipt');
			$this->db->join('tb_user','tb_receipt.id_user = tb_user.id_user');
			$this->db->join('tb_invoice','tb_receipt.id_invoice = tb_invoice.id_invoice');
			return $this->db->get()->result();
		}
		function insert($id_user,$id_invoice,$url,$deskripsi,$created_at){
			$data = array("id_user"=>$id_user,"deskripsi"=>$deskripsi,"id_invoice"=>$id_invoice,"file_receipt"=>$url,"created_at"=>$created_at);
			return $this->db->insert('tb_receipt',$data);
		}
		function edit($id_receipt,$id_user,$id_invoice,$url,$deskripsi,$updated_at){
			$data = array("id_user"=>$id_user,"deskripsi"=>$deskripsi,"id_invoice"=>$id_invoice,"file_receipt"=>$url,"updated_at"=>$updated_at);
			$this->db->where('id_receipt',$id_receipt);
			return $this->db->update('tb_receipt',$data);
		}		
		function detail($id){
			$this->db->select('tb_receipt.*,tb_user.nama,tb_invoice.no_invoice');
			$this->db->from('tb_receipt');
			$this->db->join('tb_user','tb_receipt.id_user = tb_user.id_user');
			$this->db->join('tb_invoice','tb_receipt.id_invoice = tb_invoice.id_invoice');
			$this->db->where('tb_receipt.id_receipt',$id);
			return $this->db->get()->result();
		}
		function delete($id){
			$this->db->where('id_receipt',$id);
			return $this->db->delete('tb_receipt');
		}
		function listReceiptClient($id){
			$this->db->select('tb_receipt.*,tb_user.nama,tb_invoice.no_invoice');
			$this->db->from('tb_receipt');
			$this->db->join('tb_user','tb_receipt.id_user = tb_user.id_user');
			$this->db->join('tb_invoice','tb_receipt.id_invoice = tb_invoice.id_invoice');
			$this->db->where('tb_invoice.id_client',$id);
			return $this->db->get()->result();			
		}
	}	
?>