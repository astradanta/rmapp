<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_sop extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('m_pekerjaan_client','',TRUE);
		}
		function initialize($id_daftar_pekerjaan_client){
			
			$this->db->where("id_daftar_pekerjaan_client",$id_daftar_pekerjaan_client);
			$data = $this->db->get('tb_kerja')->result();
			if(sizeof($data)==0){
				$this->insertKerja($id_daftar_pekerjaan_client);
			} else {
				return true;
			}
		}
		function insertKerja($idPekerjaan){
			$startDate = date("Y-m-d");
			$deadline = null;
			$created_at = date("Y-m-d h:m:s");
			$periode = null;
			$data = array("id_daftar_pekerjaan_client"=>$idPekerjaan,"start_date"=>$startDate,"deadline"=>$deadline,"created_at"=>$created_at,"status"=>"on progress","periode"=>$periode);
			$insert =  $this->db->insert('tb_kerja',$data);
			$id = $this->db->insert_id();
			$data = $this->m_pekerjaan_client->allBool(4);
			foreach ($data as $key) {
				$id_detail = $key->id_detail_pekerjaan;
				$this->m_pekerjaan_client->initializeBool($id,$id_detail);
			}
			return $insert;
		}
		function changeStatus($id,$status){
			$data = array("tb_kerja.status"=>$status);
			$this->db->where('tb_kerja.id_kerja',$id);
			$update = $this->db->update('tb_kerja',$data);
			return $update;
		}
		function display($id_client,$id_pekerjaan){
			$this->db->select('tb_kerja.*');
			$this->db->from('tb_kerja');
			$this->db->join('tb_daftar_pekerjaan_client','tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id_client);
			$this->db->where('tb_daftar_pekerjaan_client.id_pekerjaan',$id_pekerjaan);
			return $this->db->get()->result();
		}
		function cekSop($id_daftar_pekerjaan_client){
			$this->db->where("id_daftar_pekerjaan_client",$id_daftar_pekerjaan_client);
			return sizeof($this->db->get('tb_kerja')->result());
		}
		
	}
?>