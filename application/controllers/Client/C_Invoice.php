<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Invoice extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_invoice','',TRUE)		;
		$this->load->model('m_client','',TRUE);
	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/invoice');
		$this->load->view('Client/static/footer');			
	}
	function list()	{
		if (isset($_POST['baselink'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);			
			$data = $this->m_invoice->clientList($client[0]->id_client);
			echo json_encode($data);
		}
	}
	function new()	{
		if (isset($_POST['baselink'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);			
			$data = $this->m_invoice->clientNewList($client[0]->id_client);
			echo json_encode($data);
		}
	}		
}
