$( document ).ready(function() {
	var baselink = $("#baselink").val();
	loadData();
    function loadData(){
		$.ajax({
			type: "POST",
			url: baselink+'log/loadData',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$.each(data,function(i,item){
					if($(location).attr("href")==(baselink+'log')){
						addItemList2(item,count);

					}else{
						addItemList(item,count);
					}
					count++;
				});
				if($(location).attr("href")==(baselink+'log')){
				$('#example2').DataTable({
			      'paging'      : true,
			      'lengthChange': true,
			      'searching'   : true,
			      'ordering'    : true,
			      'info'        : true,
			      'autoWidth'   : false
			    });					
				}else{
				$('#tableLog').DataTable({
			      'paging'      : true,
			      'lengthChange': false,
			      'searching'   : false,
			      'ordering'    : false,
			      'info'        : false,
			      'autoWidth'   : false
			    });					
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});    	
    }
    function addItemList(data,count){
    	var text = '<tr>'+
	             		'<td>'+data.activity+'</td>'+
	             		'<td><small class="label label-danger"><i class="fa fa-clock-o"></i>&nbsp;'+data.date+'</small></td>'+
					'</tr>';
    	$("#listView").append(text);
    }
        function addItemList2(data,count){
    	var text = '<tr>'+
	            		'<td>'+count+'</td>'+
	             		'<td>'+data.nama+'</td>'+
	             		'<td>'+data.activity+'</td>'+
	             		'<td>'+data.date+'</td>'+
					'</tr>';
    	$("#listView").append(text);
    }
});