$(document).ready(function(){
	$('#signout').click(function(){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'logout',
				cache: false,
				success: function(response){
					data = jQuery.parseJSON(response);
					if (data.status == 1){
						window.location.replace(baselink+'login');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	});	
	$(".sidebar-menu").on('click','a',function(){
		window.location.replace($(this).attr('href'));
	})
	$("#editFormGlobal").submit(function(e){
		e.preventDefault();
		var baselink = $('#baselink').val();
		if ($("#editPasswordProfile").val()!=$("#editRetypeProfile").val()){
			alert("Password tidak sama");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'user/editUser',
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						location.reload();
						alert("Sukses merubah data profile");
					}else{
						alert("Gagal merubah data profile");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});	
		}
	});
});