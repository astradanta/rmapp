<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Hrd extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_hrd','',TRUE);

	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/hrd');
		$this->load->view('Client/static/footer');			
	}
	function list(){
		if (isset($_SESSION['idClient'])) {
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$data = $this->m_hrd->userDataLaopranHRD($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo json_encode($data);
		}		
	}
}
