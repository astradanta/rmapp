<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Recruit extends CI_Controller {
	protected $statusLabel = ["Menunggu Persetujuan","Disetujui","Tidak Disetujui","Selesai"];

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_invoice','',TRUE)		;
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_recruit','',TRUE);
	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/recruit');
		$this->load->view('Client/static/footer');			
	}	

	function list(){
		if(isset($_SESSION['idClient'])){
				$id = $_SESSION['idClient'];
				$client = $this->m_client->getClientByUser($id);
				$id_client = $client[0]->id_client;
				$data = $this->m_recruit->list($id_client);
				foreach ($data as $key) {
					$time = strtotime($key->created_at);
					$key->created_at = date('m-d-Y',$time);
					$time = strtotime($key->deadline);
					$key->deadline = date('m/d/Y',$time);					
					$key->statusLabel = $this->statusLabel[$key->status];
		
				}
				echo json_encode($data);
			}
	}
	function workingView(){
		if(isset($_POST['baselink'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$data = $this->m_pekerjaan_client->getListPekerjaanClient($id_client);
			echo json_encode($data);
		}
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_POST['access'])&&isset($_SESSION['idClient'])){
			$client = $this->m_client->getClientByUser($_SESSION['idClient']);
			$id_client = $client[0]->id_client;
			$posisi = $_POST['posisi'];
			$jumlah = $_POST['jumlah'];
			$gaji = $_POST['gaji'];
			$syarat = ($_POST['syarat']);
			$time = strtotime($_POST['deadline']);
			$deadline = date("Y-m-d",$time);
			$insert = $this->m_recruit->add($posisi,$jumlah,$gaji,$syarat,$id_client,$deadline);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Tambah request recruitment  '.$posisi,$date);
			}
		}
		echo(json_encode($result));
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$delete = $this->m_recruit->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Hapus Request ',$date);				
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_SESSION['idClient'])){
			$id = $_POST['id'];
			$request = $this->m_recruit->detail($id);
			foreach ($request as $key) {

					$time = strtotime($key->created_at);
					$key->created_at = date('m-d-Y',$time);
					$time = strtotime($key->deadline);
					$key->deadline = date('m/d/Y',$time);					
					$key->statusLabel = $this->statusLabel[$key->status];
		
				
			}
			echo json_encode($request);
		}
	}
	function edit(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])){
			$id_request = $_POST['idDetail'];
			$posisi = $_POST['posisi'];
			$jumlah = $_POST['jumlah'];
			$gaji = $_POST['gaji'];
			$syarat = ($_POST['syarat']);
			$time = strtotime($_POST['deadline']);
			$deadline = date("Y-m-d",$time);
			$edit = $this->m_recruit->edit($posisi,$jumlah,$gaji,$syarat,$id_request,$deadline);
			if ($edit) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Edit Request '.$posisi,$date);				
			}				
		}
		echo json_encode($result);
	}
}
