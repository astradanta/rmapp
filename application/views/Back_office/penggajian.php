 <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Penggajian <?php echo($namaKlien); ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo(base_url()."service/".$this->uri->segment(2)); ?>"><?php echo $namaKlien; ?></a></li>
        <li class="active">Penggajian</li>
        <input type="hidden" name="" value="<?php echo($id_client); ?>" id="idClient">
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding-top: 20px">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active" id="tabBar1"><a href="#tab_1" data-toggle="tab">Data Absensi</a></li>
              <li id="tabBar2"><a href="#tab_2" data-toggle="tab">Slip Gaji</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1" >           
                <div class="row" id="listPanel">
                  <div class="col-md-12">
                  <table id="tableAbsensi" class="table table-bordered table-striped">
                    <thead id="headAbsensi">
                          <tr>
                            <th width="20%">Tanggal</th>
                            <th width="20%">Periode</th>
                            <th width="20%">Catatan</th>
                            <th width="20%">Status</th>
                            <th width="75">Aksi</th>
                          </tr>
                    </thead>
                    <tbody id="listView">
                    	
                    </tbody>
                  </table>                  
                  </div>
                </div> 
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="row">                    
                    <div class="col-xs-12" id="workingView">
                        <div class="col-md-12" style="padding-bottom: 10px;">
                          <button class="pull-right btn btn-success" type="button" id="addButton" data-toggle="modal" data-target="#manipulateModal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
                        </div>
                        <div class="row" id="listPanel">
                          <div class="col-md-12">
                          <table id="tableSlip" class="table table-bordered table-striped">
                            <thead id="headSlip">
                                  <tr>
                                    <th width="25%">Tanggal</th>
                                    <th width="25%">Periode</th>
                                    <th width="25%">Catatan</th>
                                    <th width="75">Aksi</th>
                                  </tr>
                            </thead>
                            <tbody id="slipView">
                              
                            </tbody>
                          </table>                  
                          </div>
                        </div>                    
                    </div>
                  </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>              
        </div>
      </div>
    </section>		
</div>
<div id="lockedAction" style="display: none;">
  <button class="btn btn-info" style="margin: 5px" id="itemDownload"><i class="fa fa-download"></i> </button>
  <button class="btn btn-warning" style="margin: 5px" id="itemEdit"><i class="fa fa-pencil"></i>&nbsp; Status</button>
</div>   
<div id="openAction" style="display: none;">
  <button class="btn btn-info" style="margin: 5px" id="itemDownload"><i class="fa fa-download"></i> </button>
  <button class="btn btn-warning" style="margin: 5px" id="itemEdit"><i class="fa fa-pencil"></i>&nbsp; Status</button>
</div>
<div id="slipAction" style="display: none;">
    <button class="btn btn-warning" style="margin: 5px" id="itemEdit"><i class="fa fa-pencil"></i></button>
    <button class="btn btn-danger" style="margin: 5px" id="itemDelete"><i class="fa fa-trash"></i></button>
    <button class="btn btn-info" style="margin: 5px" id="itemDownload"><i class="fa fa-download"></i></button>
    <button class="btn btn-success" style="margin: 5px" id="itemFinish"><i class="fa fa-check"></i>&nbsp;Selesai</button>
</div>   

<div class="modal fade" id="statusAbsensiModal">
  <form action="<?php echo base_url(); ?>penggajian/change" method="post" id="statusAbsensiForm">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Verifikasi</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="id_kerja" value="">
                  <input type="hidden" name="id_client" value="<?php echo($id_client); ?>">
                  <div class="form-group">
                    <label>Sudah diverifikasi ?</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="firstRadio" name="status" value="1">Ya</label>
                          <label class="radio-inline"><input type="radio" id="secondRadio" name="status" value="0">Tidak</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="changeButton" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Form Tambah Data Slip Gaji</h3>
              </div>
              <form action="<?php echo(base_url()) ?>penggajian/add" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <input type="hidden" name="id_detail_kerja" id="idDetailSlip">
                    <label for="exampleInputEmail1">Periode</label>
                    <select class="form-control select2" id="selectPicker" name="id_kerja" style="width: 100%;">
                      <option value="0">Pilih Periode</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Catatan</label>
                      <textarea id="catatan" name="catatan" rows="10" cols="80">
                      </textarea>
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">URL</label>
                    <input type="text" class="form-control" name="url" id="url" placeholder="" required="">
                  </div>                                   
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>        
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>