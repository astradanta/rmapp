<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Recruitment
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Recruitment</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding-top: 20px">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Daftar Request</a></li>
              <li><a href="#tab_2" data-toggle="tab">Working Board</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1" >
                <div class="row">
                  <div class="col-md-12" id="viewContainer">
                    <div class="col-md-12" style="padding-bottom: 20px;"><button type="button" class="pull-right btn btn-success" id="addButton"><i class="fa fa-plus"></i>&nbsp;Tambah</button></h3></div>
                  <table id="tableRequest" class="table table-bordered table-striped">
                    <thead>
                          <tr>
                            <th width="15%">Posisi</th>
                            <th width="10%">Gaji</th>
                            <th width="12%">Jumlah</th>
                            <th width="20%">Syarat/Ketentuan</th>
                            <th width="12%">Deadline</th>
                            <th width="10%">Status</th>                            
                            <th width="10%">Progress</th> 
                            <th width="70">Aksi</th>
                          </tr>
                    </thead>
                    <tbody id="listView">
                    </tbody>
                  </table>                  
                  </div>
                  <div id="addContainer" class="col-md-12" style="display: none;">
                    <form action="<?php echo base_url(); ?>client/recruit/add" method="post" id="manipulationForm">
                      <div class="box box-primary">
                        <div class="box-header">
                          <h3 class="box-title col-md-12" id="titleForm">Tambah Request</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <div class="col-md-6">
                            <input type="hidden" name="idDetail" id="idDetail">
                            <input type="hidden" name="access" value="1">
                            <div class="form-group">
                              <label>Posisi</label>
                              <input type="text" name="posisi" id="posisi" autocomplete="off" required="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Jumlah</label>
                              <input type="text" name="jumlah" id="jumlah" autocomplete="off" required="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Gaji</label>
                              <input type="text" name="gaji" id="gaji" autocomplete="off" required="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Deadline</label>
                              <input type="text" name="deadline" id="deadline" autocomplete="off" required="" class="form-control">
                            </div>                                            
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Syarat/Ketentuan</label>
                                <textarea id="syarat" name="syarat" rows="10" cols="80">

                                </textarea>
                            </div>                       
                          </div>
                        </div>
                        <div class="box-footer" style="text-align: right;">

                          <button type="button" class="btn btn-danger" id="btn_cancel">Cancel</button>                
                          <button type="submit" class="btn btn-success" id="btn_save" >Simpan</button>
                        </div>
                        <!-- /.box-body -->
                      </div>
                    </form>                            
                  </div>
                </div>  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="row">                    
                    <div class="col-xs-12" id="workingView">
                    
                    </div>
                  </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>              
        </div>      	
      </div>
    </section>
</div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div style="display: none;" id="listItemTemplate">
            <div class="col-md-6 col-xs-12">
              <div class="info-box bg-aqua infoBox" style="cursor: pointer;" >
                <span class="info-box-icon infoBoxIcon" style="" id="itemLeftIcon" data-id=""><i style="font-size: 40px;">100<sup>%</sup></i></span>

                <div class="info-box-content ">
                  <div style="display: flex; height: 44px;" id="itemContainer" data-id="">
                    <span style="white-space: normal;" class="info-box-text col-xs-11" id="namaPekerjaan">SPT Masa (bulanan)</span>
                    <span class="info-box-number col-xs-1"><i style="line-height: 44px" class="pull-right fa fa-fw fa-arrow-right"></i></span>
                  </div>
                  <div class="progress col-xs-12">
                    <div class="progress-bar" style="width: 70%"></div>
                  </div>
                  <div class="col-xs-11" style="display: flex">
                    <span class="info-box-text col-xs-11" id="itemlistDeadline">2018-06-17</span>
                  </div>
                  
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>  
</div> 
<div class="modal fade" id="detailModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
              </div>
              <form action="" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Posisi</label>
                    <input type="text" class="form-control" name="nama" id="detailPosisi" placeholder="Ketikan Nama" required="" disabled="">
                  </div>
                  <div class="form-group">
                    <label>Gaji </label>
                     <input type="text" class="form-control" name="nama" id="detailGaji" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div> 
                  <div class="form-group">
                    <label>Jumlah </label>
            <input type="text" class="form-control" name="nama" id="detailJumlah" placeholder="Ketikan Nama" required="" disabled="">  
                  </div>                                                                                          
                  <div class="form-group">
                    <label for="exampleInputPassword1">Syarat dan Ketentuan</label>
                    <div class="custom-form-control" id="detailSyarat"></div>
                  </div>
                  <div class="form-group">
                    <label>Status </label>
                    <input type="text" class="form-control" name="nama" id="detailStatus" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Deadline</label>
            <input type="text" class="form-control" name="nama" id="detailDeadline" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Dibuat Pada </label>
            <input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
            <input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div>                  
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                     <label id="editGenerate"></label>                    
                  </div>                                    
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>          