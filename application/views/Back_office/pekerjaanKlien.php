<div class="content-wrapper">
	<section class="content-header">
      <h1>
        <?php echo $namaKlien; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $namaKlien; ?></li>
        <input type="hidden" name="" id="idPekerjaanKlien" value="<?php echo($idPekerjaanKlien) ?>">
      </ol>
      <div class="col-xs-12 contentHeader"></div>
    </section>
	 <section class="content">
      <div class="row">
      	<div class="col-md-12" style="padding-top: 20px">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Working Board</a></li>
              <li><a href="#tab_2" data-toggle="tab">Overview</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1" >
				<div class="row">
			 		<div class="col-xs-12">
			 			<button class="btn btn-success pull-right" data-toggle="modal" data-target="#manipulateModal" style="margin-bottom: 10px;"><i class="fa fa-plus" ></i> Tambah</button>
			 		</div>
			 		<div class="col-xs-12" id="listView">
					
			 		</div>
			 	</div>	
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <div class="row" id="contentOverView">
					 <div class="col-md-4">
					          <!-- Widget: user widget style 1 -->
					          <div class="box box-widget widget-user timbul">
					            <!-- Add the bg color to the header using any of the bg-* classes -->
					            <div class="widget-user-header bg-aqua-active">
					              <h3 class="widget-user-username"><?php echo($namaKlien) ?></h3>
					            </div>
					            <div class="widget-user-image">
					              <img class="img-circle" src="<?php echo(base_url().$client->logo_client) ?>" alt="User Avatar" style="height: 80px;width: 80px;">
					            </div>
					            <div class="box-footer" style="background-color: #f8f8f8">
					              <div class="row">
					                <!-- /.col -->
					              </div>
					              <!-- /.row -->
					            </div>
					          </div>
					          <div class="box box-primary timbul">
					            <div class="box-header">
					              <h3 class="box-title">List Pekerjaan</h3>
					            </div>
					            <!-- /.box-header -->
					            <div class="box-body">
					            	<input type="hidden" id="idClient" name="" value="<?php echo $client->id_client ?>">
					              <table id="example2" class="table table-bordered table-striped">
					                <thead>
					                <tr>
					                  <th width="10%">No</th>
					                  <th width="90%">Pekerjaan</th>
					                </tr>
					                </thead>
					                <tbody id="listPekerjaan">
					               
					                </tbody>
					              </table>
					            </div>
					            <!-- /.box-body -->
					          </div>					          
					          <!-- /.widget-user -->
					 </div> 
				        <div class="col-md-8">
				          
				          <!-- /.box -->

				          <div class="box box-primary timbul">
				            <div class="box-header">
				              <h3 class="box-title">Detail Info</h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body">
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Jenis Perusahaan</label>
				                  <input type="text" class="form-control" name="jenis" id="inputJenis" disabled="" value="<?php echo($client->jenis_perusahaan)?>" >
				                </div>
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Alamat</label>
				                  <textarea id="inputAlamat" name="alamat" class="form-control" disabled=""><?php echo($client->alamat)?></textarea>
				                </div>
				               	<div class="form-group">
				                  <label for="exampleInputEmail1">No Telepon</label>
				                  <input type="text" class="form-control" name="telepon" id="inputPhone" disabled="" value="<?php echo($client->no_telepon)?>">
				                </div>
				                <div class="form-group" >
				                  <label for="exampleInputEmail1">Email</label>
				                  <input type="email" class="form-control" name="email" id="inputEmail" disabled="" value="<?php echo($client->email)?>">
				                </div>
				                <div class="form-group">
				                  <label for="exampleInputEmail1">Deskripsi</label>
				                  <textarea id="inputDeskripsi" name="deskripsi" class="form-control" disabled=""><?php echo($client->deskripsi)?></textarea>
				                </div>
				                <div class="form-group" >
				                  <label for="exampleInputEmail1">Mulai Kontrak</label>
				                  <input type="text" name="startContract" class="form-control" id="startContract" value="<?php echo($client->start_contract)?>" disabled="">
				                </div>
				               	<div class="form-group" >
				                  <label for="exampleInputEmail1">Selesai Kontrak</label>
				                  <input type="text" name="endContract" class="form-control" id="endContract" value="<?php echo($client->end_contract)?>" disabled="">
				                </div>   			                				            	
				            </div>
				            <!-- /.box-body -->
				          </div>
				          <!-- /.box -->
				        </div>					                 		
                  </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>      		
      	</div>
      </div>	 	
<!-- 	 	<div class="row">
	 		<div class="col-xs-12">
	 			<h4 class="col-xs-10">Working Board</h4>
	 			<button class="btn btn-success pull-right" data-toggle="modal" data-target="#manipulateModal" style="margin-bottom: 10px;"><i class="fa fa-plus" ></i> Tambah</button>
	 		</div>
	 		<div class="col-xs-12" id="listView">
			
	 		</div>
	 	</div> -->
	 </section>
</div>
<div style="display: none;" id="listItemTemplate">
		        <div class="col-md-6 col-xs-12">
		          <div class="info-box bg-aqua infoBox" style="cursor: pointer;" >
		            <span class="info-box-icon infoBoxIcon" style="" id="itemLeftIcon" data-id=""><i style="font-size: 40px;">100<sup>%</sup></i></span>

		            <div class="info-box-content ">
		              <div style="display: flex; height: 44px;" id="itemContainer" data-id="">
			              <span style="white-space: normal;" class="info-box-text col-xs-11" id="namaPekerjaan">SPT Masa (bulanan)</span>
			              <span class="info-box-number col-xs-1"><i style="line-height: 44px" class="pull-right fa fa-fw fa-arrow-right"></i></span>
		              </div>
		              <div class="progress col-xs-12">
		                <div class="progress-bar" style="width: 70%"></div>
		              </div>
		              <div class="col-xs-11" style="display: flex">
		              	<span class="info-box-text col-xs-11" id="itemlistDeadline">2018-06-17</span>
		              	<div class="pull-right"><a href="" style="margin-left: 30px;margin-right: 30px;" id="editItem" data-id=""><i class="fa fa-pencil"></i></a></div>	
		              	<div class="pull-right"><a href="" ><i class="fa fa-trash" id="deleteItem" data-id=""></i></a></div>
		              </div>
		              
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div> 	
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title" id="modalTitle">Tambah Data Detail Pekerjaan</h3>
	            </div>
	            <form action="<?php echo(base_url()) ?>pekerjaanKlien/add" method="post" id="manipulateForm" enctype="multipart/form-data">
	              <div class="box-body">
	              	<input type="hidden" id="idDetail" name="idDetail" value="">
	              	<input type="hidden" name="access" value="1">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Pekrjaan</label>
		                <select class="form-control select2" id="inputPekerjaan" name="pekerjaan" style="width: 100%;">
		                </select>
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Tanggal Mulai</label>
	                  <input type="text" class="form-control" id="startDate" placeholder="Ketikan Nama" name="startDate" required="" autocomplete="off">	                 
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deadline</label>
	                  <input type="text" class="form-control" id="deadLine" placeholder="Ketikan Nama" name="deadLine" required="" autocomplete="off">	                 
	                </div>		              	                               	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>        