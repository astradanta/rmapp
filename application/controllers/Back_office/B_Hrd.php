<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Hrd extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_penggajian','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_hrd','',TRUE);
	}
	public function index()
	{
		$idPekerjaan = $this->uri->segment(2);
		$temData = $this->m_client->getDetailClient($idPekerjaan);
		$data["id_client"] = $idPekerjaan;
		$data["namaKlien"] = $temData[0]->nama_client;
		$data['client'] = $temData[0];
		$this->load->view('Back_office/static/header',$data);
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/hrd');
		$this->load->view('Back_office/static/footer');
		if(!isset($_SESSION['nama'])){
			redirect(base_url().'login');
		}
	}
	
	function list(){
		if (isset($_SESSION['id'])) {
			$id_client = $_POST['id_client'];
			$data = $this->m_hrd->adminDataLaopranHRD($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo json_encode($data);
		}
	}	
	function detail(){
		if(isset($_SESSION['id'])){		
			$id_client = $_POST['id_client'];
			$id_kerja = $_POST['id_kerja'];
			$data = $this->m_hrd->adminLaporanHRDDetail($id_client,$id_kerja);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
				$time = strtotime($key->deadline);
				$key->deadline = date('m/d/Y',$time);
			}
			echo(json_encode($data));
		}
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_SESSION['id'])){
			$id_client = $_POST['id_client'];
			$periode = $_POST['periode'];
			$pekerjaanKlien = $this->m_pekerjaan_client->getPekerjaanKlienByuser($id_client,3);			
			$id_daftar_pekerjaan_client = $pekerjaanKlien[0]->id_daftar_pekerjaan_client;
			$deadLine = null;
			$startDate = date("Y-m-d");
			$created_at = date("Y-m-d h:m:s");
			$url = $_POST['url'];
			$catatan = $_POST['catatan'];
			$insert = $this->m_penggajian->addDetailKerja($id_daftar_pekerjaan_client,$startDate,$deadLine,$created_at,$periode);
			$inizialise =  $this->m_pekerjaan_client->initializeBool($insert,7,1,$url,$catatan);
			if($insert && $inizialise){
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}	
	function edit(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_client = $_POST['id_client'];
			$deadLine = null;
			$startDate = date("Y-m-d");
			$updated_at = date("Y-m-d");
			$url = $_POST['url'];
			$catatan = $_POST['catatan'];
			$id_kerja = $_POST["id_kerja"];
			$periode = $_POST['periode'];
			$url = $_POST['url'];
			$edit = $this->m_hrd->adminLaporanHRDEdit($id_kerja,$startDate,$deadLine,$updated_at,$periode,$catatan,$url);
			if($edit){
				$result["status"] = "success";
			}
		}
		echo(json_encode($result));
	}
	function delete(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id_kerja = $_POST['id'];
			$delete_kerja = $this->m_hrd->deleteKerja($id_kerja);
			if($delete_kerja){
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}
	function change(){
		$result["status"] = "failed";
		if(isset($_SESSION['id'])){
			$id = $_POST['id_kerja'];
			$change = $this->m_pekerjaan_client->changeStatus($id);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Mengganti status pekerjaan menjadi selesai',$date);
			}
		}
		echo(json_encode($result));		
	}	
}