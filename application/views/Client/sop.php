
<div class="content-wrapper">
	<section class="content-header">
      <h1>
        SOP
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">SOP</li>
        <input type="hidden" name="idClient" id="id_client" value="<?php echo $id_client; ?>">
        <input type="hidden" name="idPekerjaan" id="id_pekerjaan" value="<?php echo $id_pekerjaan; ?>">
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
	 <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding: 20px;">
          <!-- Custom Tabs -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">SOP</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                      <span class="" style="margin-left: 20px;margin-right: 20px; color: green" id="lblStartdate"> Start Date </span>                
                  </div>
                  <div class="col-md-6">                                            
                      <span class="pull-right" style="margin-left: 20px;margin-right: 20px;" id="lblStatus"> Status : On Progress </span>
                  </div>
                </div>
                <div class="row" style="padding-top: 20px;">
                    <div class="col-sm-6">
                      <h4>File SOP</h4>
                      <div class="col-md-12" style=" background-color: #ecf0f5;" id="containerSumber">
                        
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <h4>Pekerjaan</h4>
                      <div class="col-md-12" style="background-color: #ecf0f5;" id="containerPekerjaan">
                       
                      </div>                     
                    </div>                    
                </div>
            </div>
            <!-- /.box-body -->
          </div>          
        </div>
        <!-- /.col -->
      </div>
	 </section>
</div>

<div id="withData" style="display: none;">
                   <div class="col-md-12" style="padding: 20px">
                        <span class="pull-right" style="margin-left: 20px;margin-right: 20px;" id="lblStatus"> Status :On Progress </span>
                        <span class="pull-right" style="margin-left: 20px;margin-right: 20px; color: red" id="lblDeadline"> Deadline </span>
                        <span class="pull-right" style="margin-left: 20px;margin-right: 20px; color: green" id="lblStartdate"> Start Date </span>
                    </div>
                    <div class="col-sm-6">
                      <h4>Sumber Data</h4>
                      <div class="col-md-12" style=" background-color: #ecf0f5;" id="containerSumber">
                        
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <h4>Pekerjaan</h4>
                      <div class="col-md-12" style="background-color: #ecf0f5;" id="containerPekerjaan">
                       
                      </div>                     
                    </div>  
</div>
<div id="noData" style="display: none;">
                      <div class="col-md-12">
                        <h4 style="text-align: center;">Tidak ada pekerjaan</h4>
                      </div>  
</div>
<div id="sumberNoItem" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-xs-8 col-sm-9 col-md-10" id="leftName">Tahap 1</h5>
                                  <button id="uploadButton" data-kerja="" data-pekerjaan="" type="button" class="btn btn-success pull-right uploadButton" id="leftAction">Upload</button>
                              </div>

                            </div>                            
                          </div>  
</div>
<div id="sumberWithItem" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-xs-8 col-sm-9 col-md-10" id="leftName">Tahap 2</h5>
                                  <button id="editButton" data-kerja="" data-pekerjaan="" type="button" class="btn btn-warning pull-right editButton" id="leftAction">Edit</button>
                              </div>

                            </div>                            
                          </div>
</div>
<div id="pekerjaanNoItem" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-xs-8 col-sm-9 col-md-10" id="leftName">Tahap 2</h5>
                                  <h5 style="color: red" class="pull-right">No File</h5>
                              </div>

                            </div>                            
                          </div>  
</div>
<div id="pekerjaanWithItem" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-xs-8 col-sm-9 col-md-10" id="leftName">Tahap 3</h5>
                                  <button id="downloadButton" data-kerja="" data-pekerjaan="" type="button" class="btn btn-primary pull-right downloadButton" id="leftAction">Download</button>
                              </div>

                            </div>                            
                          </div> 
</div>
<div id="pekerjaanBoolNo" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-md-8" id="leftName">Tahap 2</h5>
                                  <h5 class="pull-right label label-danger" id="boolStatus">Belum</h5>
                              </div>

                            </div>                            
                          </div>  
</div>
<div id="pekerjaanBoolYes" style="display: none;">
                          <div class="col-xs-12">
                            <div class="bottomBorder">
                              <div class="row">
                                  <h5 class="col-md-8" id="leftName">Tahap 2</h5>
                                  <h5 class="pull-right label label-success" id="boolStatus">Sudah</h5>
                              </div>

                            </div>                            
                          </div>
</div> 
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Upload sumber data</h3>
              </div>
              <form action="<?php echo(base_url()) ?>manageKerja/add" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="idDetail" value="">
                  <input type="hidden" name="access" value="1">
                  <input type="hidden" name="id_kerja" id="input_id_kerja" value="">
                  <input type="hidden" name="id_pekerjaan" id="input_id_pekerjaan" value="">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Upload URL</label>
                    <input type="text" class="form-control" name="url" id="inputUrl" placeholder="Ketikan upload url" required="">
                  </div>                              
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<div id="archiveWithItem" style="display: none;">
                       <div class="col-md-12">
                         <table id="example2" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>No</th>
                            <th>Pekerjaan/Sumber Data</th>
                            <th>Di Upload Pada</th>
                            <th>Di Re-upload Pada</th>
                            <th>Di Re-upload Oleh</th>
                            <th>Link</th>
                          </tr>
                          </thead>
                          <tbody id="listView">

                          </tbody>
                        </table>                       
                      </div>
</div>
<div id="archiveNoItem" style="display: none;">
                      <div class="col-md-12">
                        <h4 style="text-align: center;">Theres no Archive data</h4>
                      </div>  
</div>
<div class="modal fade" id="changeModal">
  <form action="<?php echo base_url(); ?>manageKerja/changeDetail" method="post" id="changeForm">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Pekerjaan</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="id_detail_kerja" name="id_detail_kerja" value="">
                  <input type="hidden" name="id_client" value="<?php echo($id_client); ?>">
                  <div class="form-group">
                    <label>Status Pekerjaan</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="firstRadio" name="status" value="1">Sudah</label>
                          <label class="radio-inline"><input type="radio" id="secondRadio" name="status" value="0">Belum</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="changeButton" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>
<div class="modal fade" id="globalChangeModal">
  <form action="<?php echo base_url(); ?>sop/change" method="post" id="GlobalChangeForm">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Ubah Status Pekerjaan</h3>
              </div>
                <div class="box-body">
                  <input type="hidden" id="idKerja" name="id" value="">
                  <input type="hidden" name="id_client" value="<?php echo($id_client); ?>">
                  <div class="form-group">
                    <label>Status Pekerjaan</label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="thirdRadio" name="status" value="selesai">Selesai</label>
                          <label class="radio-inline"><input type="radio" id="fourthRadio" name="status" value="on progress">On Progress</label>
                      </div>  

                  </div>                               
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" id="changeButtonGlobal" class="btn btn-success pull-right">Simpan</button>
                </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>        