<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_recruit extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function list($id){
			$this->db->where('id_client',$id);
			return $this->db->get('tb_request_recruit')->result();
		}
		function add($posisi,$jumlah,$gaji,$syarat,$id_client,$deadline){
			$created_at = date('Y-m-d');
			$data = array("id_client"=>$id_client,"posisi"=>$posisi,"jumlah"=>$jumlah,"gaji"=>$gaji,"syarat"=>$syarat,"deadline"=>$deadline,"created_at"=>$created_at);
			return $this->db->insert('tb_request_recruit',$data);
		}
		function delete($id){
			$this->db->where('id_request',$id);
			return $this->db->delete('tb_request_recruit');
		}
		function detail($id){
			$this->db->where('id_request',$id);
			return $this->db->get('tb_request_recruit')->result();
		}
		function edit($posisi,$jumlah,$gaji,$syarat,$id_request,$deadline){
			$updated_at = date('Y-m-d');
			$data = array("posisi"=>$posisi,"jumlah"=>$jumlah,"gaji"=>$gaji,"syarat"=>$syarat,"deadline"=>$deadline,"updated_at"=>$updated_at);
			$this->db->where('id_request',$id_request);
			return $this->db->update('tb_request_recruit',$data);
		}
		function change($id_request,$status){
			$data = array("status"=>$status);
			$this->db->where('id_request',$id_request);
			return $this->db->update('tb_request_recruit',$data);
		}
		function getRecruitByIdKerja($id){
			$this->db->select('tb_kerja.*,tb_request_recruit.posisi');
			$this->db->from('tb_kerja');
			$this->db->join('tb_request_recruit','tb_kerja.id_request = tb_request_recruit.id_request');
			$this->db->where('tb_kerja.id_kerja',$id);
			return $this->db->get()->result();
		}
	}	
?>