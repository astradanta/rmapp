-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2018 at 05:19 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_red_consulting`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_archive_invoice`
--

CREATE TABLE `tb_archive_invoice` (
  `id_archive` int(10) UNSIGNED NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nama_client` varchar(225) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `note` text,
  `status` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_client`
--

CREATE TABLE `tb_client` (
  `id_client` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `nama_client` varchar(225) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(50) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `deskripsi` text,
  `logo_client` varchar(225) DEFAULT NULL,
  `start_contract` date DEFAULT NULL,
  `end_contract` date DEFAULT NULL,
  `jenis_perusahaan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_daftar_pekerjaan`
--

CREATE TABLE `tb_daftar_pekerjaan` (
  `id_pekerjaan` int(10) UNSIGNED NOT NULL,
  `nama_pekerjaan` varchar(300) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(300) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_daftar_pekerjaan_client`
--

CREATE TABLE `tb_daftar_pekerjaan_client` (
  `id_daftar_pekerjaan_client` int(10) UNSIGNED NOT NULL,
  `id_client` int(10) UNSIGNED DEFAULT NULL,
  `id_pekerjaan` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_kerja`
--

CREATE TABLE `tb_detail_kerja` (
  `id_detail_kerja` int(10) UNSIGNED NOT NULL,
  `id_kerja` int(10) UNSIGNED DEFAULT NULL,
  `id_detail_pekerjaan` int(10) UNSIGNED DEFAULT NULL,
  `upload_file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_pekerjaan`
--

CREATE TABLE `tb_detail_pekerjaan` (
  `id_detail_pekerjaan` int(10) UNSIGNED NOT NULL,
  `id_pekerjaan` int(10) UNSIGNED DEFAULT NULL,
  `nama_detail_pekerjaan` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `created_at` datetime DEFAULT NULL,
  `jenis_pekerjaan` int(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_history_kerja`
--

CREATE TABLE `tb_history_kerja` (
  `id_history_kerja` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `activity` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(225) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_history_konfirmasi`
--

CREATE TABLE `tb_history_konfirmasi` (
  `id_history_konfirmasi` int(10) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nama_client` varchar(225) DEFAULT NULL,
  `no_invoice` varchar(225) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `nama_bank` varchar(225) DEFAULT NULL,
  `atas_nama_bank` varchar(225) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `id_invoice` int(10) UNSIGNED NOT NULL,
  `id_client` int(10) UNSIGNED DEFAULT NULL,
  `tanggal_invoice` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `file_invoice` varchar(255) DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kerja`
--

CREATE TABLE `tb_kerja` (
  `id_kerja` int(10) UNSIGNED NOT NULL,
  `id_pekerjaan` int(10) UNSIGNED DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi_pembayaran`
--

CREATE TABLE `tb_konfirmasi_pembayaran` (
  `id_konfirmasi` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_invoice` int(10) UNSIGNED DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `atas_nama_bank` varchar(255) DEFAULT NULL,
  `note` text,
  `status_konfirmasi` varchar(225) DEFAULT NULL,
  `file_bukti` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_log_activity`
--

CREATE TABLE `tb_log_activity` (
  `id_log_activity` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `activity` text,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id_pengumuman` int(10) UNSIGNED NOT NULL,
  `id_client` int(10) UNSIGNED DEFAULT NULL,
  `deskripsi` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_receipt`
--

CREATE TABLE `tb_receipt` (
  `id_receipt` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `deskripsi` text,
  `id_invoice` int(10) UNSIGNED DEFAULT NULL,
  `file_receipt` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `jenis_user` int(2) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `jenis_user`, `nama`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin Demo', 'astra.danta@gmail.com', 'LP6F1FNvndr040KtMqbV2tzsmQ4M4F+CiHktVRSW2Ntd7Kvf/5zClIo4ouYsv76lZd+r7gGxUJJr336ECSdBSg==', '2018-06-27 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_archive_invoice`
--
ALTER TABLE `tb_archive_invoice`
  ADD PRIMARY KEY (`id_archive`);

--
-- Indexes for table `tb_client`
--
ALTER TABLE `tb_client`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `user_client` (`id_user`);

--
-- Indexes for table `tb_daftar_pekerjaan`
--
ALTER TABLE `tb_daftar_pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `tb_daftar_pekerjaan_client`
--
ALTER TABLE `tb_daftar_pekerjaan_client`
  ADD PRIMARY KEY (`id_daftar_pekerjaan_client`),
  ADD KEY `rc_daftar_kerja_client` (`id_client`),
  ADD KEY `rc_daftar_kerja_pekerjaan` (`id_pekerjaan`);

--
-- Indexes for table `tb_detail_kerja`
--
ALTER TABLE `tb_detail_kerja`
  ADD PRIMARY KEY (`id_detail_kerja`);

--
-- Indexes for table `tb_detail_pekerjaan`
--
ALTER TABLE `tb_detail_pekerjaan`
  ADD PRIMARY KEY (`id_detail_pekerjaan`),
  ADD KEY `rc_pekerjan_detail` (`id_pekerjaan`);

--
-- Indexes for table `tb_history_kerja`
--
ALTER TABLE `tb_history_kerja`
  ADD PRIMARY KEY (`id_history_kerja`);

--
-- Indexes for table `tb_history_konfirmasi`
--
ALTER TABLE `tb_history_konfirmasi`
  ADD PRIMARY KEY (`id_history_konfirmasi`);

--
-- Indexes for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indexes for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  ADD PRIMARY KEY (`id_kerja`);

--
-- Indexes for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `tb_log_activity`
--
ALTER TABLE `tb_log_activity`
  ADD PRIMARY KEY (`id_log_activity`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `tb_receipt`
--
ALTER TABLE `tb_receipt`
  ADD PRIMARY KEY (`id_receipt`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_archive_invoice`
--
ALTER TABLE `tb_archive_invoice`
  MODIFY `id_archive` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_client`
--
ALTER TABLE `tb_client`
  MODIFY `id_client` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_daftar_pekerjaan`
--
ALTER TABLE `tb_daftar_pekerjaan`
  MODIFY `id_pekerjaan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_daftar_pekerjaan_client`
--
ALTER TABLE `tb_daftar_pekerjaan_client`
  MODIFY `id_daftar_pekerjaan_client` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_kerja`
--
ALTER TABLE `tb_detail_kerja`
  MODIFY `id_detail_kerja` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_detail_pekerjaan`
--
ALTER TABLE `tb_detail_pekerjaan`
  MODIFY `id_detail_pekerjaan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_history_kerja`
--
ALTER TABLE `tb_history_kerja`
  MODIFY `id_history_kerja` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_history_konfirmasi`
--
ALTER TABLE `tb_history_konfirmasi`
  MODIFY `id_history_konfirmasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  MODIFY `id_invoice` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  MODIFY `id_kerja` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  MODIFY `id_konfirmasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_log_activity`
--
ALTER TABLE `tb_log_activity`
  MODIFY `id_log_activity` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id_pengumuman` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_receipt`
--
ALTER TABLE `tb_receipt`
  MODIFY `id_receipt` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_client`
--
ALTER TABLE `tb_client`
  ADD CONSTRAINT `user_client` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_daftar_pekerjaan_client`
--
ALTER TABLE `tb_daftar_pekerjaan_client`
  ADD CONSTRAINT `rc_daftar_kerja_client` FOREIGN KEY (`id_client`) REFERENCES `tb_client` (`id_client`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rc_daftar_kerja_pekerjaan` FOREIGN KEY (`id_pekerjaan`) REFERENCES `tb_daftar_pekerjaan` (`id_pekerjaan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_detail_pekerjaan`
--
ALTER TABLE `tb_detail_pekerjaan`
  ADD CONSTRAINT `rc_pekerjan_detail` FOREIGN KEY (`id_pekerjaan`) REFERENCES `tb_daftar_pekerjaan` (`id_pekerjaan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
