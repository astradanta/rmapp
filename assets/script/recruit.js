$(document).ready(function(){
	var baselink = $("#baselink").val();
		id_client = $("#idClient").val();
	list();

	displayList()
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'recruit/list/'+$("#idClient").val(),
			cache: false,
			success: function(response){
				$("#listView").html('');
				var data = jQuery.parseJSON(response);
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableRequest").DataTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){		
		var action = '<button id="viewItem" type="button" data-id="'+data.id_request+'" class="btn btn-info" style="margin:5px;"><i class="fa fa-eye"></i></button>';
                     
        if(data.status == 0) action +='<button type="button" id="approveItem" data-id="'+data.id_request+'" data-deadline="'+data.deadline+'" class="btn btn-success approveItem" style="margin:5px;"><i class="fa fa-check"></i></button>';
		var text = "<tr>"+						
						"<td>"+data.posisi+"</td>"+
						"<td>"+data.gaji+"</td>"+
						"<td>"+data.jumlah+"</td>"+
						"<td>"+data.syarat+"</td>"+
						"<td>"+data.deadline+"</td>"+
						"<td>"+data.statusLabel+"</td>"+						
						"<td style='text-align:center;'>"+action+"</td>"+
					"</tr>";
		$("#listView").append(text);
	}
	$("#listView").on('click','.approveItem',function(){
		var id = $(this).attr('data-id');
			deadline = $(this).attr('data-deadline');
		$("#idDetail").val(id);
		$("#deadline").val(deadline);
		$("#manipulateModal").modal('show');
	})
	function displayList(){

		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/list',
			data:{"baselink":baselink,"id":$("#idClient").val()},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);
				$("#workingView").html('');
				$.each(data,function(i,item){
					parseListWorking(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseListWorking(data){
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find("#itemLeftIcon").html('<i style="font-size: 40px;">'+persentase+'<sup>%</sup></i>')
		$("#listItemTemplate").find("#itemContainer").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan);		
		$("#listItemTemplate").find("#itemlistDeadline").html(data.deadline);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#namaPekerjaan").html(data.posisi);
		$("#workingView").append($("#listItemTemplate").html());
	}
	$("#manipulateForm").submit(function(e){
			e.preventDefault();
			if(($("#firstRadio").prop('checked')==false) && ($("#secondRadio").prop('checked')==false)){
				alert("pilih salah satu status");
			}  else {
					$.ajax({
					type: "POST",
					url: $(this).prop("action"),
					data: new FormData( this ),
			      	processData: false,
			      	contentType: false,
					success: function(response){
						var data = jQuery.parseJSON(response)
						if(data.status == "success"){
							alert("Status Berhasil Diganti");
							$("#tableRequest").DataTable().destroy();
							list();
							displayList();
							$("#manipulateModal").modal('hide');
						} else {
							alert("Status Ganti Diganti")
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
				        console.log(xhr.status);
				        console.log(xhr.responseText);
				        console.log(thrownError);
				    }
				});			
			}
			
		});	
	$('#workingView').on('click','#itemContainer',function(e) { 
			var url = baselink+"manageKerja/"+$(this).attr('data-id')+"/"+id_client+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#workingView').on('click','#itemlistDeadline',function(e) { 
			var url = baselink+"manageKerja/"+$(this).attr('data-id')+"/"+id_client+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#workingView').on('click','#itemLeftIcon',function(e) { 
			var url = baselink+"manageKerja/"+$(this).attr('data-id')+"/"+id_client+"/"+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});
	$("#listView").on('click','#viewItem',function(){
		var id = $(this).attr('data-id');
		detail(id);
		$("#detailModal").modal("show");
	});	
	function detail(id){
		$.ajax({
			type: "POST",
			url: baselink+'recruit/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseDetail(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data){
			$("#detailPosisi").val(data[0].posisi);
			$("#detailGaji").val(data[0].gaji);
			$("#detailJumlah").val(data[0].jumlah);
			$("#detailSyarat").html(data[0].syarat);
			$("#detailStatus").val(data[0].statusLabel);
			$("#detailDeadline").val(data[0].deadline);
			$("#detailCreateAt").val(data[0].created_at);
			$("#detailUpdateAt").val(data[0].updated_at);
	}								
});