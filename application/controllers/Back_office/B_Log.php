<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Log extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		//$this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/log');
		$this->load->view('Back_office/static/footer2');
		if(!isset($_SESSION['nama'])){
			redirect(base_url().'login');
		}
	}

	function loadData()	{
		$id = $_SESSION['id'];
		$data = $this->m_log->loadLogUser($id);
		echo json_encode($data);
	}
	function currentLog(){
		$id = $_SESSION['id'];
		$data = $this->m_log->currentLog($id);
		echo json_encode($data);		
	}
}