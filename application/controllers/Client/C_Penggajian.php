<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Penggajian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_penggajian','',TRUE);

	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/penggajian');
		$this->load->view('Client/static/footer');			
	}

	function add(){
		$result['status'] = "failed";
		if(isset($_SESSION['idClient'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$pekerjaanKlien = $this->m_pekerjaan_client->getPekerjaanKlienByuser($id_client,2);
			$periode = $_POST['periode'];
			$id_daftar_pekerjaan_client = $pekerjaanKlien[0]->id_daftar_pekerjaan_client;
			$time = strtotime($_POST['deadline']);
			$deadLine = date('Y-m-d',$time);
			$startDate = date("Y-m-d");
			$created_at = date("Y-m-d");
			$url = $_POST['url'];
			$catatan = $_POST['catatan'];
			$insert = $this->m_penggajian->addDetailKerja($id_daftar_pekerjaan_client,$startDate,$deadLine,$created_at,$periode);
			$inizialise =  $this->m_pekerjaan_client->initializeBool($insert,5,0,$url,$catatan);
			if($insert && $inizialise){
				$result['status'] = "success";
			}
		}
		echo(json_encode($result));
	}

	function list(){
		if(isset($_SESSION['idClient'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$data = $this->m_penggajian->userDataAbsensi($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo(json_encode($data));
		}
	}

	function detail(){
		if(isset($_SESSION['idClient'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$id_kerja = $_POST['id'];
			$data = $this->m_penggajian->userAbsensiDetail($id_client,$id_kerja);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
				$time = strtotime($key->deadline);
				$key->deadline = date('m/d/Y',$time);
			}
			echo(json_encode($data));
		}
	}
	function edit(){
		$result["status"] = "failed";
		if(isset($_SESSION['idClient'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$time = strtotime($_POST['deadline']);
			$deadLine = date('Y-m-d',$time);
			$startDate = date("Y-m-d");
			$updated_at = date("Y-m-d");
			$url = $_POST['url'];
			$catatan = $_POST['catatan'];
			$id_kerja = $_POST["id_kerja"];
			$periode = $_POST['periode'];
			$url = $_POST['url'];
			$edit = $this->m_penggajian->userAbsensiEdit($id_kerja,$startDate,$deadLine,$updated_at,$periode,$catatan,$url);
			if($edit){
				$result["status"] = "success";
			}
		}
		echo(json_encode($result));
	}
	function delete(){
		$result["status"] = "failed";
		if(isset($_SESSION['idClient'])){
			$id_kerja = $_POST['id'];
			$delete_kerja = $this->m_penggajian->deleteKerja($id_kerja);
			if($delete_kerja){
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}
	function listSlip(){
		if(isset($_SESSION['idClient'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);
			$id_client = $client[0]->id_client;
			$data = $this->m_penggajian->userDataSlip($id_client);
			foreach ($data as $key) {
				if($key->updated_at != null){
					$time = strtotime($key->updated_at);
					$key->tanggal = date('d-m-Y',$time);
				} else {
					$time = strtotime($key->created_at);
					$key->tanggal = date('d-m-Y',$time);
				}
				if($key->note == null){
					$key->note = "";
				}
			}
			echo(json_encode($data));
		}		
	}
}
