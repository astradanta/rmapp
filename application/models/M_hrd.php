<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_hrd extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function adminDataLaopranHRD($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 3 AND tb_kerja.status = "on progress"';
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function userDataLaopranHRD($id_client){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 3';
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function adminLaporanHRDDetail($id_client,$id_kerja){
			$sql = 'SELECT tb_kerja.*,tb_detail_kerja.note,tb_detail_kerja.upload_file,tb_detail_kerja.status as "pengerjaan_status" FROM tb_kerja INNER join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_detail_kerja ON tb_kerja.id_kerja = tb_detail_kerja.id_kerja WHERE tb_daftar_pekerjaan_client.id_client = '.$id_client.' AND tb_daftar_pekerjaan_client.id_pekerjaan = 3 AND tb_kerja.id_kerja = '.$id_kerja;
			$query = $this->db->query($sql);
			return $query->result();			
		}
		function adminLaporanHRDEdit($id_kerja,$startDate,$deadLine,$updated_at,$periode,$note,$url){
			$data = array("start_date"=>$startDate,"deadline"=>$deadLine,"updated_at"=>$updated_at,"periode"=>$periode);
			$this->db->where("id_kerja",$id_kerja);
			$edit = $this->db->update("tb_kerja",$data);
			if($edit){

				$this->editDetailKerja($id_kerja,$note,$url);
			}
			return $edit;
		}
		function editDetailKerja($id_kerja,$note,$url){
			$data = array("note"=>$note,"upload_file"=>$url);
			$this->db->where("id_kerja",$id_kerja);
			$this->db->update("tb_detail_kerja",$data);
		}
		function deleteKerja($id_kerja){
			$detail = $this->deleteDetail($id_kerja);
			$delete = 0;
			if($detail){
				$this->db->where("id_kerja",$id_kerja);
				$delete = $this->db->delete("tb_kerja");
			}
			return $delete;
		}
		function deleteDetail($id_kerja){
			$this->db->where("id_kerja",$id_kerja);
			return $this->db->delete("tb_detail_kerja");
		}						
	}
?>