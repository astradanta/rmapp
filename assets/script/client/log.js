$( document ).ready(function() {
	var baselink = $("#baselink").val();
	loadData();
	console.log()
    function loadData(){
		$.ajax({
			type: "POST",
			url: baselink+'log/loadData',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$.each(data,function(i,item){
					addItemList(item,count);
					count++;
				});
				$('#example2').DataTable({
			      'paging'      : true,
			      'lengthChange': true,
			      'searching'   : true,
			      'ordering'    : true,
			      'info'        : true,
			      'autoWidth'   : false
			    });
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});    	
    }
    function addItemList(data,count){
    	var text = '<tr>'+
	            		'<td>'+count+'</td>'+
	             		'<td>'+data.nama+'</td>'+
	             		'<td>'+data.activity+'</td>'+
	             		'<td>'+data.date+'</td>'+
					'</tr>';
    	$("#listView").append(text);
    }
});