<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Receipt
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Payment</li>
        <li class="active">Receipt</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12" style="padding-top: 20px">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Receipt</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row" id="contentInvoice">
                       <div class="col-md-12">
                          <button type="button" class="btn btn-success pull-right" id="btnAddInvoice" style="margin-bottom: 20px" data-toggle="modal" data-target="#manipulateModal"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
                       </div>
                       <div class="col-md-12">
                         <table id="tableInvoice" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th width="15%">Nama Admin</th>
                            <th width="15%">No Invoice</th>
                            <th width="20%">Deskripsi</th>
                            <th width="15%">Dibuat Pada</th>
                            <th width="15%">Diubah Pada</th>                           
                            <th width="150">Aksi</th>
                          </tr>
                          </thead>
                          <tbody id="listView">

                          </tbody>
                        </table>                       
                      </div>
                </div>
                <!-- /.box-body -->
              </div>                       
            </div>                
        </div>
      </div>
    </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title" id="modalTitle">Tambah Data Receipt</h3>
	            </div>
	            <form action="<?php echo(base_url()) ?>receipt/add" method="post" id="manipulateForm" enctype="multipart/form-data">
	              <div class="box-body">
	              	<input type="hidden" id="idDetail" name="idDetail" value="">
	              	<input type="hidden" name="access" value="1">
		              <div class="form-group">
		                <label>Invoice</label>
		                <select class="form-control select2" id="selectPicker" name="pekerjaan" style="width: 100%;">
		                	<option value="0">Pilih Invoice</option>
		                </select>
		              </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Deskripsi</label>
                    <textarea id="inputDeskripsi" name="deskripsi" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Receipt File</label>
                    <input type="text" class="form-control" name="url" id="inputUrl" placeholder="" required="" autocomplete="off">
                  </div>                           
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
